#!/bin/sh

./ci/prepare-docker.sh

CONTAINER_IMAGE=registry.gitlab.com/${CI_PROJECT_PATH}
IMAGE_VERSION=$([ -n "${CI_COMMIT_TAG}" ] && echo "${CI_COMMIT_TAG#*v}" || echo "${CI_COMMIT_REF_NAME##*/}")

echo "Fetching latest tag from registry (caching optimization)"
docker pull ${CONTAINER_IMAGE}:latest || true

echo "Build docker image and tag as latest"
docker build --cache-from ${CONTAINER_IMAGE}:latest --tag ${CONTAINER_IMAGE}:latest .

echo "Push latest tag"
docker push ${CONTAINER_IMAGE}:latest;

if [ -n "${CI_COMMIT_TAG}" ]; then
  echo "Git tag found: ${CI_COMMIT_TAG}"
 
  echo "Tagging docker image as git tag: ${CI_COMMIT_TAG}"
  docker tag ${CONTAINER_IMAGE}:latest ${CONTAINER_IMAGE}:${CI_COMMIT_TAG}
  
  echo "Pushing docker tag ${CI_COMMIT_TAG}"
  docker push ${CONTAINER_IMAGE}:${CI_COMMIT_TAG}
fi
